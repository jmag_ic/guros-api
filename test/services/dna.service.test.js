const assert = require('assert');
const sinon = require('sinon');

const dnaService = require('../../app/services/dna.service');
const dnaRepository = require('../../app/repositories/dna.repository');

describe('Mutation service', () => {
  describe('has mutation method', () => {
    it('should return true when dna has two row sequences', async () => {
      // given
      const dna = [
        'ATGCAA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCCA',
        'TCACTG'
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, true);
    });

    it('should return true when dna has two col sequences', async () => {
      // given
      const dna = [
        'ATGCGA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCAGA',
        'TCACTG'
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, true);
    });

    it('should return true when dna has a row and a col sequence', async () => {
      // given
      const dna = [
        'ATGCGA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCTA',
        'TCACTG'
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, true);
    });

    it('should return true when dna has two diagonal L-R sequences', async () => {
      // given
      const dna = [
        "ATGTGA",
        "CGTCGC",
        "TTGATT",
        "AGAGGG",
        "GCGTGA",
        "TCATTG"
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, true);
    });

    it('should return true when dna has two diagonal R-L sequences', async () => {
      // given
      const dna = [
        "ATGTTA",
        "CATTGC",
        "TTTATT",
        "ATAGGG",
        "TCGTGA",
        "TCATTA"
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, true);
    });

    it('should return true when dna has a diagonal L-R and a diagonal R-L sequence', async () => {
      // given
      const dna = [
        "ATGTGA",
        "CATCGC",
        "TTGATT",
        "TGAGGG",
        "GCGTGA",
        "TCATTG"
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, true);
    });

    it('should return false when dna has no sequence', async () => {
      // given
      const dna = [
        "ATGCGA",
        "CAGTGC",
        "TTATTT",
        "AGACGG",
        "GCGTCA",
        "TCACTG"
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, false);
    });

    it('should return false when dna has one sequence', async () => {
      // given
      const dna = [
        "ATGCGA",
        "CAGTGC",
        "GTATTT",
        "AGACGG",
        "GCGTCA",
        "TCAGTG"
      ];
      // when
      const hasMutation = await dnaService.hasMutation(dna);
      // then
      assert.strictEqual(hasMutation, false);
    });
  });

  describe('find mutation method', () => {
    it('should insert dna if does not exists in database', async () => {
      // given
      const dna = [
        'ATGCAA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCCA',
        'TCACTG'
      ]

      const dnaDbData = {
        dna: dna.join(','),
        hasMutation: true
      }

      const getDnaStub = sinon.stub(dnaService, 'getDna');
      getDnaStub.withArgs(dna)
        .resolves(null);
      
      const hasMutationStub = sinon.stub(dnaService, 'hasMutation');
      hasMutationStub.withArgs(dna)
        .resolves(true);

      const saveDnaStub = sinon.stub(dnaService, 'saveDna');
      saveDnaStub.withArgs(dna, true)
        .resolves(dnaDbData);

      // when
      const hasMutation = await dnaService.findMutation(dna);
      
      getDnaStub.restore();
      hasMutationStub.restore();
      saveDnaStub.restore();

      // then
      sinon.assert.calledOnceWithExactly(getDnaStub, dna);
      sinon.assert.calledOnceWithExactly(hasMutationStub, dna);
      sinon.assert.calledOnceWithExactly(saveDnaStub, dna, true);
      assert.strictEqual(hasMutation, dnaDbData.hasMutation);

    });

    it('should not insert dna if exists in database', async () => {
      // given
      const dna = [
        'ATGCAA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCCA',
        'TCACTG'
      ]

      const dnaDbData = {
        dna: dna.join(','),
        hasMutation: true
      }

      const getDnaStub = sinon.stub(dnaService, 'getDna');
      getDnaStub.withArgs(dna)
        .resolves(dnaDbData);

      const hasMutationStub = sinon.stub(dnaService, 'hasMutation');
      hasMutationStub.withArgs(dna)
        .resolves(true);
      
      const saveDnaStub = sinon.stub(dnaService, 'saveDna');
      saveDnaStub.withArgs(dna, true)
        .resolves(dnaDbData);

      // when
      const hasMutation = await dnaService.findMutation(dna);

      getDnaStub.restore();
      hasMutationStub.restore();
      saveDnaStub.restore();

      // then
      sinon.assert.calledOnceWithExactly(getDnaStub, dna);
      sinon.assert.notCalled(hasMutationStub);
      sinon.assert.notCalled(saveDnaStub);
      assert.strictEqual(hasMutation, dnaDbData.hasMutation);
    });
  });

  describe('get dna method', async () => {
    it('should retrive a dna stored data if dna exists', async () => {
      // given
      const dna = [
        'ATGCAA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCCA',
        'TCACTG'
      ]
      const dnaKey = dna.join(','); 
      const dnaDbData = {
        dna: dnaKey,
        hasMutation: true
      }

      const getStub = sinon.stub(dnaRepository, 'get');
      getStub.withArgs(dnaKey).resolves(dnaDbData);

      // when
      const retrievedDnaData = await dnaService.getDna(dna);

      getStub.restore();

      // then
      sinon.assert.calledOnceWithExactly(getStub, dnaKey);
      assert.strictEqual(retrievedDnaData, dnaDbData);

    });

    it('should retrive null if dna not exists', async () => {
      // given
      const dna = [
        'ATGCAA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCCA',
        'TCACTG'
      ]
      const dnaKey = dna.join(',');

      const getStub = sinon.stub(dnaRepository, 'get');
      getStub.withArgs(dnaKey).resolves(null);

      // when
      const retrievedDnaData = await dnaService.getDna(dna);

      getStub.restore();

      // then
      sinon.assert.calledOnceWithExactly(getStub, dnaKey);
      assert.strictEqual(retrievedDnaData, null);
    });
  });

  describe('save dna method', async () => {
    it('should save dna data', async () => {
      // given
      const dna = [
        'ATGCAA',
        'CCGTGC',
        'TTATGT',
        'AGAAGG',
        'CCCCCA',
        'TCACTG'
      ]
      const dnaKey = dna.join(','); 
      const dnaDbData = {
        dna: dnaKey,
        hasMutation: true
      }

      const saveStub = sinon.stub(dnaRepository, 'save');
      saveStub.withArgs(dnaDbData).resolves(dnaDbData);

      // when
      const savedDnaData = await dnaService.saveDna(dna, true);

      saveStub.restore();

      // then
      sinon.assert.calledOnceWithExactly(saveStub, dnaDbData);
      assert.strictEqual(savedDnaData, dnaDbData);

    });
  });

  describe('get stats method', async () => {
    it('should retrieve ratio when mutation and no mutation records exists', async () => {
      // given
      const countByHasMutationStub = sinon.stub(dnaRepository, 'countByHasMutation');
      countByHasMutationStub.withArgs(true).resolves(40);
      countByHasMutationStub.withArgs(false).resolves(100);

      // when
      const stats = await dnaService.getStats();

      countByHasMutationStub.restore();

      // then
      sinon.assert.calledWithExactly(countByHasMutationStub, true);
      sinon.assert.calledWithExactly(countByHasMutationStub, false);
      assert.deepStrictEqual(stats, {
        count_mutations: 40,
        count_no_mutation: 100,
        ratio: 0.4
      });
    });

    it('should retrieve zero ratio when mutation records does not exists', async () => {
      // given
      const countByHasMutationStub = sinon.stub(dnaRepository, 'countByHasMutation');
      countByHasMutationStub.withArgs(true).resolves(0);
      countByHasMutationStub.withArgs(false).resolves(100);

      // when
      const stats = await dnaService.getStats();

      countByHasMutationStub.restore();

      // then
      sinon.assert.calledWithExactly(countByHasMutationStub, true);
      sinon.assert.calledWithExactly(countByHasMutationStub, false);
      assert.deepStrictEqual(stats, {
        count_mutations: 0,
        count_no_mutation: 100,
        ratio: 0
      });
    });

    it('should retrieve zero ratio when no mutation records does not exists', async () => {
      // given
      const countByHasMutationStub = sinon.stub(dnaRepository, 'countByHasMutation');
      countByHasMutationStub.withArgs(true).resolves(40);
      countByHasMutationStub.withArgs(false).resolves(0);

      // when
      const stats = await dnaService.getStats();

      countByHasMutationStub.restore();

      // then
      sinon.assert.calledWithExactly(countByHasMutationStub, true);
      sinon.assert.calledWithExactly(countByHasMutationStub, false);
      assert.deepStrictEqual(stats, {
        count_mutations: 40,
        count_no_mutation: 0,
        ratio: 0
      });
    });
  });
});
