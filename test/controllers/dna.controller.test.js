const sinon = require('sinon');

const dnaService = require('../../app/services/dna.service');
const dnaController = require('../../app/controllers/dna.controller');

describe('Dna controller', () => {
  describe('POST mutation', () => {
    it('should return status 200 if DNA has mutation', async () => {
      // given
      const findMutation = sinon.stub(dnaService, 'findMutation').resolves(true);
      const req = {
        body: { dna: ['GTC', 'ACT', 'TCA'] }
      };
      const res = {
        status: sinon.stub().returns({ send: sinon.spy() })
      };

      //when
      await dnaController.findMutation(req, res);
      findMutation.restore();

      // then
      sinon.assert.calledOnce(findMutation);
      sinon.assert.calledWith(res.status, 200);
    });

    it('should return status 403 if DNA does not has mutation', async () => {
      // given
      const findMutation = sinon.stub(dnaService, 'findMutation').resolves(false);
      const req = {
        body: { dna: ['GTC', 'ACT', 'TCA'] }
      };
      const res = {
        status: sinon.stub().returns({ send: sinon.spy() })
      };

      //when
      await dnaController.findMutation(req, res);
      findMutation.restore();

      // then
      sinon.assert.calledOnce(findMutation);
      sinon.assert.calledWith(res.status, 403);
    });
  });

  describe('Get stats', () => {
    it('should return stats', async () => {
      // given
      const statsStub = {
        count_mutations: 40,
        count_no_mutation: 100,
        ratio: 0.4
      }
      const getStats = sinon.stub(dnaService, 'getStats').resolves(statsStub);
      
      const req = {};
      const spy = sinon.spy();
      const res = {
        status: sinon.stub().returns({ json: spy })
      };

      //when
      await dnaController.getStats(req, res);
      getStats.restore();

      // then
      sinon.assert.calledOnce(getStats);
      sinon.assert.calledWith(res.status, 200);
      sinon.assert.calledWithExactly(spy, statsStub);
    });
  });
});