# Guros REST API - Prueba Técnica
# Requerimientos
- Nodejs
- NPM
- AWS Credentials
## Configuración de credenciales
Se necesita configurar credenciales de AWS para conectar a la base de datos DynamoDb
### Opción 1. Configuración en archivo
Crear archivo 'credentials'
```bash
$ touch ~/.aws/credentials
```
Añadir contenido al archivo con las respectivas credenciales de AWS
```
[default]
aws_access_key_id = XXXXXXXX
aws_secret_access_key = XXXXXXXX
```
### Opción 2. Exportar variables de entorno
```
$ export AWS_ACCESS_KEY_ID = XXXXXXXX
$ export AWS_SECRET_ACCESS_KEY = XXXXXXXX
```

# Ejecución local
### Instalar paquetes
```bash
$ npm install
```
### Ejecución de pruebas
```bash
$ npm test
```
### Despliegue de desarrollo
Se despliega un ambiente de desarrollo en AWS y se crea la base de datos
```bash
$ npm run dev-deploy
```
### Iniciar servidor en entorno local
```bash
$ npm start
```
# REST API en producción
## API URLS
- API URL: https://xkpsghf2d2.execute-api.us-east-1.amazonaws.com/prod/
- Mutation POST URL: https://xkpsghf2d2.execute-api.us-east-1.amazonaws.com/prod/mutation
- Stats GET URL: https://xkpsghf2d2.execute-api.us-east-1.amazonaws.com/prod/stats
# Stack
- Serverless
- Express
- DynamoDb
