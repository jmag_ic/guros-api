const express = require('express');
const helmet = require('helmet');
const serverless = require('serverless-http');

const appRouter = require('./app.router');

const app = express();

// Add json and urlencoded parsers
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Add helmet to avoid HTTP Headers vulnerabilities
app.use(helmet());

// Add application routes
app.use(appRouter);
app.all('*', (req, res) => res.send('404 Page not found'));

module.exports.handler = serverless(app);