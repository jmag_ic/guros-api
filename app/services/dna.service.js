const dnaHelper = require('../helpers/dna.helper');
const dnaRepository = require('../repositories/dna.repository');

/**
 * Find for a processed dna in data base if exists return their has mutation result,
 * otherwise process given dna, store results in data base and returns it
 * @param {Array<string>} dna 
 * @returns {Promise<boolean>}
 */
exports.findMutation = async (dna) => {
  let dnaData = await this.getDna(dna);

  if (dnaData == null || dnaData == undefined) {
    hasMutation = await this.hasMutation(dna);
    dnaData = await this.saveDna(dna, hasMutation);
  }

  return dnaData.hasMutation;
}

/**
 * Retrieves a stored dna data if exists
 * @param {Array<string>} dna 
 * @returns {Promise<{ dna: string, hasMutation: boolean }>}
 */
exports.getDna = (dna) => {
  const dnaKey = dna.join(',')

  return dnaRepository.get(dnaKey);
}

/**
 * Retrieve mutations stats based on stored dna data
 * @returns {Promise<{ count_mutations: number, count_no_mutation: number, ratio: number }>}
 */
exports.getStats = async () => {
  const countMutations = await dnaRepository.countByHasMutation(true);
  const countNoMutations = await dnaRepository.countByHasMutation(false);
  const ratio = countNoMutations ? (countMutations / countNoMutations) : 0;

  return {
    count_mutations: countMutations,
    count_no_mutation: countNoMutations,
    ratio
  }
}

/**
 * Determines if the DNA strand has a mutation
 * @param {Array<string>} dna 
 * @returns {Promise<boolean>}
 */
exports.hasMutation = (dna) => {
  return new Promise((resolve) => {
    
    let sequences = 0;

    for (let i = 0; i < dna.length; i++) {
      for (let j = 0; j < dna[i].length; j++) {

        // If the position of the current element allows it:
        // Find a horizontal sequence, if exist and is a second sequence found then resolves true
        if (j < (dna[i].length - 3)
          && dnaHelper.findHSeq(dna, i, j)
          && (++sequences > 1)) {
          resolve(true);
        }

        // If the position of the current element allows it:
        // Find vertical or diagonal sequence, if exist and is a second sequence found then resolves true
        if (i < (dna.length - 3)
          && (dnaHelper.findVSeq(dna, i, j)
            || (j < (dna[i].length - 3) && dnaHelper.findDLRSeq(dna, i, j))
            || (j > 2 && dnaHelper.findDRLSeq(dna, i, j)))
          && (++sequences > 1)
        ) {
          resolve(true);
        }
      }
    }
    resolve(false);
  });
}

/**
 * Store dna data in database
 * @param {Array<string>} dna 
 * @param {boolean} hasMutation 
 * @returns {Promise<{dna: string, hasMutation: boolean}>}
 */
exports.saveDna = async (dna, hasMutation) => {
  const dnaKey = dna.join(',')

  return dnaRepository.save({
    dna: dnaKey,
    hasMutation
  });
}