const AWS = require('aws-sdk');
const client = new AWS.DynamoDB.DocumentClient();

exports.client = client;

/**
 * export table names defined in env(serverless.yml)
 */
exports.tables = {
    dna: process.env.DNA_TABLE_NAME
}