/**
 * Find a secuence of 4 identical dna chars horizontally
 *    j
 *   ooooo
 * i oxxxx
 *   ooooo
 *   ooooo
 *   ooooo
 * @param {Array<string>} dna 
 * @param {number} i 
 * @param {number} j 
 * @returns {boolean}
 */
exports.findHSeq = (dna, i, j) => {
    return checkSequence(dna[i].charAt(j) + dna[i].charAt(j + 1) + dna[i].charAt(j + 2) + dna[i].charAt(j + 3));
}

/**
 * Find a secuence of 4 identical dna chars vertically
 *    j
 *   ooooo
 * i oxooo
 *   oxooo
 *   oxooo
 *   oxooo
 * @param {Array<string>} dna 
 * @param {number} i 
 * @param {number} j 
 * @returns {boolean}
 */
exports.findVSeq = (dna, i, j) => {
    return checkSequence(dna[i].charAt(j) + dna[i + 1].charAt(j) + dna[i + 2].charAt(j) + dna[i + 3].charAt(j));
}

/**
 * Find a secuence of 4 identical dna chars diagonally lef to right
 *    j
 *   ooooo
 * i oxooo
 *   ooxoo
 *   oooxo
 *   oooox
 * @param {Array<string>} dna 
 * @param {number} i 
 * @param {number} j 
 * @returns {boolean}
 */
exports.findDLRSeq = (dna, i, j) => {
    return checkSequence(dna[i].charAt(j) + dna[i + 1].charAt(j + 1) + dna[i + 2].charAt(j + 2) + dna[i + 3].charAt(j + 3));
}


/**
 * Find a secuence of 4 identical dna chars diagonally right to left
 *      j
 *   ooooo
 * i oooxo
 *   ooxoo
 *   oxooo
 *   xoooo
 * @param {Array<string>} dna 
 * @param {number} i 
 * @param {number} j 
 * @returns {boolean}
 */
exports.findDRLSeq = (dna, i, j) => {
    return checkSequence(dna[i].charAt(j) + dna[i + 1].charAt(j - 1) + dna[i + 2].charAt(j - 2) + dna[i + 3].charAt(j - 3));
}

checkSequence = (sequence) => {
    return new RegExp(/[\A]{4}|[\G]{4}|[\T]{4}|[\C]{4}/).test(sequence);
}