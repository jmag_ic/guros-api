
exports.dnaValidator = async(req, res, next) => {
    const { body } = req;

    const validDnaMatcher = new RegExp(/^[\T\G\C\A]*$/gm);

    if(body.hasOwnProperty('dna')
        && Array.isArray(body['dna'])
        && validDnaMatcher.test(body['dna'].join(''))) {
        next();
    } else {
        return res.status(400).send();
    }
}