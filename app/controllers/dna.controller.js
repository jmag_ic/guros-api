const dnaService = require('../services/dna.service');

exports.findMutation = async (req, res) => {
    const { dna } = req.body;
    
    const hasMutation = await dnaService.findMutation(dna);

    res.status(hasMutation ? 200 : 403).send();
}

exports.getStats = async (req, res) => {
    const stats = await dnaService.getStats();

    res.status(200).json(stats);
}