const { client, tables } = require('../helpers/db.helper');

exports.save = (dnaData) => {
    return client.put({
        TableName: tables.dna,
        Item: dnaData
    }).promise().then(() => dnaData);
}

exports.get = (dna) => {
    return client.get({
        TableName: tables.dna,
        Key: { dna }
    }).promise().then(({ Item }) => Item);
}

exports.countByHasMutation = (hasMutation) => {
    return client.scan({
        TableName: tables.dna,
        FilterExpression: 'hasMutation = :hasMutation',
        ExpressionAttributeValues: {
            ':hasMutation': hasMutation
        },
        Select: 'COUNT'
    }).promise().then(({ Count }) => Count);
}