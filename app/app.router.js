const express = require('express');

const router = express.Router();

const mutationMdlw = require('../app/middlewares/mutation.middleware');
const dnaController = require('./controllers/dna.controller');

router.post('/mutation', mutationMdlw.dnaValidator, dnaController.findMutation);
router.get('/stats', dnaController.getStats);

module.exports = router;